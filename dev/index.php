<?php

//  Load Composer
require dirname( dirname( __FILE__ ) ) . '/vendor/autoload.php';

//  Import Classes
use Ayedev\Macro\MacroTrait;


//  Normal Class
class SimpleClass
{
    //  Use Trait
    use MacroTrait;
}

//  Define Macro
SimpleClass::macro( 'getTime', function()
{
    //  Return
    return time();
} );

//  Create Instance
$simple = new SimpleClass;

//  Print
echo $simple->getTime();