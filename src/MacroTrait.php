<?php namespace Ayedev\Macro;

//  Define NO_RETURN_DATA
if( !defined( 'NO_RETURN_DATA' ) )  define( 'NO_RETURN_DATA', '--NULL--' );


/**
 * Trait WithMacro
 * 
 * @package developeryamhi/macros
 */
trait MacroTrait
{
    /** @var array $_macros */
    private static $_macros = [];


    /**
     * Register Macro
     *
     * @param $name
     * @param callable $callback
     */
    public static function macro( $name, callable $callback )
    {
        //  Store
        self::$_macros[$name] = $callback;
    }

    /**
     * Check is Macro Registered
     *
     * @param $name
     * @return bool
     */
    public static function hasMacro( $name )
    {
        //  Return
        return ( isset( self::$_macros[$name] ) );
    }

    /**
     * Remove the Registered Macro
     *
     * @param $name
     */
    public static function removeMacro( $name )
    {
        //  Check
        if( self::hasMacro( $name ) )
        {
            //  Remove
            unset( self::$_macros[$name] );
        }
    }

    /**
     * Process Macro
     *
     * @param $name
     * @param array $args
     * @param mixed|null $ref
     * @return mixed
     */
    protected static function processMacro( $name, $args = [], $ref = null )
    {
        //  Check
        if( self::hasMacro( $name ) )
        {
            //  Callback
            $callback = self::$_macros[$name];

            //  Check for Closure
            if( $ref && $callback instanceof \Closure )
            {
                //  Bind
                $callback = $callback->bindTo( $ref );
            }

            //  Return
            return call_user_func_array( $callback, $args );
        }

        //  Return
        return NO_RETURN_DATA;
    }


    /**
     * Listen Magic Method
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call( $name, $arguments )
    {
        //  Check
        if( self::hasMacro( $name ) )
        {
            //  Process Macro
            return self::processMacro( $name, $arguments, $this );
        }

        //  Throw Exception
        throw new \Exception( "Method [{$name}] doesn't exist on class " . get_called_class() );
    }
}