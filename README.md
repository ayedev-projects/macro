Macros
==========================

Enables dynamically adding functions to any class 

## Install:

Via composer:

```
$ composer require ayedev/macro
```


# Usage:

```php

//  Import Trait
use Ayedev\Macro\MacroTrait;

//  Normal Class
class SimpleClass
{
    //  Use Trait
    use MacroTrait;
}

//  Define Macro
SimpleClass::macro( 'getTime', function()
{
    //  Return
    return time();
} );

//  Create Instance
$simple = new SimpleClass;

//  Print
echo $simple->getTime();
//  OUTPUT =>s   1486740611

```